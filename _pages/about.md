---
layout: about
title: about
permalink: /
subtitle: PhD student at <a href='#'>Department of Astronomy & The Kavli Institute for Astronomy and Astrophysics (KIAA), Peking University</a>.

profile:
  align: right
  image: me.png
  address: >
    <p>DoA & KIAA, Peking University</p>
    <p>Yiheyuanlu 5, Haidian District</p>
    <p>Beijing 100871, CHINA</p>

news: true  # includes a list of news items
selected_papers: true # includes a list of papers marked as "selected={true}"
social: true  # includes social icons at the bottom of the page
---

My works focus on galaxy morphology and structure. I also want to explore the formation and evolution of galaxies. 

I was born in [Chaoyang](https://en.wikipedia.org/wiki/Chaoyang,_Liaoning) City, Liaoning Province, and grew up in Xizhong Island （西中岛） by the Bohai Sea in [Dalian](https://en.wikipedia.org/wiki/Dalian) City, Liaoning Province.
